@file:Suppress("DEPRECATION")

package com.example.contacts.uiview.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.contacts.R
import com.example.contacts.datamodel.model.Contact
import com.example.contacts.uiview.adapter.ContactListAdapter
import com.example.contacts.uiview.listener.RecyclerViewClickListener
import com.example.contacts.viewmodel.ContactVM
import com.example.contacts.viewmodel.factory.ContactVMFactory


class MainActivity : AppCompatActivity(), RecyclerViewClickListener {

    private val PERMISSIONS_REQUEST_READ_CONTACTS = 1
    private var dataAdapter: ContactListAdapter? = null
    private var rvContact: RecyclerView? = null
    private var contactVM: ContactVM? = null
        private var factory: ContactVMFactory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        factory = ContactVMFactory(this)
        contactVM = ViewModelProviders.of(this, factory).get(ContactVM::class.java)

        rvContact = findViewById(R.id.rv_contact)
        rvContact?.layoutManager = LinearLayoutManager(this)
        rvContact?.setHasFixedSize(true)
        rvContact?.apply {
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
        requestContactPermission()

        contactVM!!.getAllContact().observe(
            this
        ) { contacts ->
            dataAdapter = ContactListAdapter(contacts as ArrayList<Contact>,this)
            rvContact?.adapter = dataAdapter
        }
    }

    private fun requestContactPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_CONTACTS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.READ_CONTACTS
                    )
                ) {
                    val builder = AlertDialog.Builder(this)
                    builder.setTitle("Read contacts access needed")
                    builder.setPositiveButton(android.R.string.ok, null)
                    builder.setMessage("Please enable access to contacts.")
                    builder.setOnDismissListener {
                        requestPermissions(
                            arrayOf(Manifest.permission.READ_CONTACTS),
                            PERMISSIONS_REQUEST_READ_CONTACTS
                        )
                    }
                    builder.show()
                } else {
                    requestPermissions(
                        this, arrayOf(Manifest.permission.READ_CONTACTS),
                        PERMISSIONS_REQUEST_READ_CONTACTS
                    )
                }
            } else {
                contactVM!!.beginSyncContact()
            }
        } else {
            contactVM!!.beginSyncContact()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSIONS_REQUEST_READ_CONTACTS -> {
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    contactVM!!.beginSyncContact()

                } else {
                    Toast.makeText(
                        this,
                        "You have disabled a contacts permission",
                        Toast.LENGTH_LONG
                    ).show()
                }
                return
            }
        }
    }

    override fun onRecyclerViewItemClick(view: View?, listItem: Any?) {
        Intent(this, DetailContactActivity::class.java).also {
            it.putExtra("contact", (listItem as Contact))
            startActivity(it)
        }
    }


}