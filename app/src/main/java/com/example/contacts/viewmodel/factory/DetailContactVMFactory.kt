package com.example.contacts.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.contacts.datamodel.model.Contact
import com.example.contacts.viewmodel.DetailContactVM

@Suppress("UNCHECKED_CAST")
class DetailContactVMFactory(detailContact:Contact): ViewModelProvider.NewInstanceFactory() {
    private val vm = DetailContactVM(detailContact)
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return vm as T
    }
}