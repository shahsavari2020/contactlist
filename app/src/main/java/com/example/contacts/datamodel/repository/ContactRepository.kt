package com.example.contacts.datamodel.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.contacts.datamodel.local.db.AppDatabase
import com.example.contacts.datamodel.local.db.dao.ContactDao
import com.example.contacts.datamodel.model.Contact

class ContactRepository(context: Context) {
    private var mContactDao: ContactDao

    init {
        val db: AppDatabase = AppDatabase.invoke(context)
        mContactDao = db.contactDAO()
    }

    fun deleteAll() {
        mContactDao.deleteAll()
    }

    fun insert(contacts: List<Contact?>) {
        mContactDao.insert(contacts)
    }

    fun getAll(): LiveData<List<Contact>> {
        return mContactDao.fetchAll()
    }



}